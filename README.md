# [Javascript] - Ecommerce Black Friday Marvel 

## Descrição do problema

Vem chegando a Black Friday e a nossa parceira, a Marvel Comics, está precisando colocar em produção um app de exposição/vendas de suas revistas em quadrinhos. É um app básico, consiste de uma tela de listagem, uma tela de detalhamento(com opção de quantidade e botão para compra) e uma tela de checkout dos produtos selecionados pelo usuário.

## Requisitos

* **API: [https://developer.marvel.com](https://developer.marvel.com)**

* **Crie uma Single Page Application de uma loja de revistas em quadrinhos utilizando a API da Marvel para todos os consumos de dados.**

* **O layout e por sua conta, seja criativo.**

* **A arquitetura é por sua conta, seja esperto.**

* **Não esqueça da otimização de velocidade da aplicação nem da experiência do usuário.**

* **12% das revistas em quadrinhos carregadas devem ser marcadas como raras (aleatoriamente no próprio fonte ao carregar o REST).**

* **O checkout deverá conter uma área com opcão de código de desconto (pode validar mock, sem rest).**

* **(OPCIONAL) Existem dois tipos de cupons: Cupons raros e cupons comuns. Cupons comuns dão desconto somente para quadrinhos comuns enquanto raros podem ser usados em qualquer tipo.**

 | Cupom| Desconto|
 |-----------|----------|
 | Comum | 10% |
 | Raro | 25% |

* **No arquivo README do projeto explique o funcionamento e a solução adotada na sua implementação do desafio.**

# Notas

1. Utilizar o framework javascript: AngularJS 
2. Não será necessário autenticação.
3. Comente qualquer dúvida e cada decisão tomada. Você pode utilizar o README para esclarecer.
4. Entre os critérios de avaliação estão:
* Usabilidade
* Criatividade
* Qualidade do código
* Código limpo e organização
* Documentação de código
* Documentação do projeto (readme)
* Performance
* Quantidade de funcionalidades básicas e extra

